# frozen_string_literal: true

require_relative "range_list/version"

require_relative "range_list/formatter"
require_relative "range_list/utils"
require_relative "range_list/adder"
require_relative "range_list/remover"

require_relative "range_list/list"

# RangeList
module RangeList
  class Error < StandardError; end
  # Your code goes here...
end
