module RangeList
  # Adder for RangeList
  module Adder
    class << self
      # add range into list
      def add(list, range)
        range = RangeList::Formatter.validate_range(range)
        new_range = RangeList::Formatter.format_range(range)

        return [new_range] if list.empty?

        do_add([], list, new_range)
      end

      private

      def do_add(current_stack, leftover_list, new_range)
        stack = current_stack.clone
        list = leftover_list.clone

        return stack.push(new_range) if list.empty?

        new_left, new_right = new_range

        # A, B
        current_left, current_right = current_range = list.shift

        #         A -> B
        # C -> D
        return stack.push(new_range, list) if current_left > new_right

        # A -----------> B
        #     C --> D
        return leftover_list if (current_left < new_left) && (current_right > new_right)

        # A -> B
        #         C -> D
        if current_right < new_left

          stack.push(current_range)

          return do_add(stack, list, new_range)
        end

        # overlap
        #
        # Example
        #
        #    A ------> B
        # C ----> D
        #
        # or
        #
        # A ------> B
        #    C -------> D
        #
        # or
        #
        #    A ----> B
        # C -----------> D
        #
        if (current_left >= new_left) || (current_right <= new_right)
          left = [current_left, new_left].min
          right = [current_right, new_right].max

          stack.push([left, right])

          return RangeList::Utils.maybe_append_list(stack, list)
        end

        stack
      end
    end
  end
end
