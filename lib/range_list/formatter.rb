# frozen_string_literal: true

module RangeList
  # A collection of formaters for inputs
  module Formatter
    class << self
      # validate if the range is expected format
      def validate_range(range)
        case range
        in [Integer, Integer] => valid_range
          valid_range
        else
          raise <<~ERROR_MSG
            Range: #{range} is invalid.

            Example range: [1, 2]
          ERROR_MSG
        end
      end

      # format the range into correct format,
      # currently only make sure the order is correct.
      def format_range(range)
        range.sort
      end
    end
  end
end
