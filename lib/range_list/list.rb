# frozen_string_literal: true

module RangeList
  # Main class of RangeList
  #
  # A pair of integers define a range, for example: [1, 5).
  # This range includes integers: 1, 2, 3, and 4.
  #
  # A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
  class List
    # Construct a range list
    #
    # = Options
    # - :range The initial range (ex. `[1, 5]`)
    #
    # Returns a new RangeList::List instance
    def initialize(range: [])
      @list =
        if range.empty?
          []
        else
          [range]
        end
    end

    # Add a range to the current list
    #
    # = Params
    # - `range`: The range (ex. `[1, 5]`)
    #
    # Returns the current list
    def add(range)
      @list = RangeList::Adder.add(@list, range)
    end

    def remove(range)
      @list = RangeList::Remover.remove(@list, range)
    end

    # Print the list in a human-readable format
    #
    # = Example
    #     range_list = RangeList::List.new
    #     range_list.print
    #     #> "[)"
    #
    # Returns the current list in String
    def print
      return "" if @list.empty?

      @list.map do |range|
        first, last = range
        "[#{first}, #{last})"
      end.join(" ")
    end
  end
end
