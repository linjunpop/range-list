# frozen_string_literal: true

module RangeList
  # The module to remove a range from the list
  module Remover
    class << self
      def remove(list, range)
        range = RangeList::Formatter.validate_range(range)
        range_to_remove = RangeList::Formatter.format_range(range)

        return [] if list.empty?

        do_remove([], list, range_to_remove)
      end

      private

      def do_remove(current_stack, leftover_list, range_to_remove)
        stack = current_stack.clone
        list = leftover_list.clone

        return stack.push(range_to_remove) if list.empty?

        new_left, new_right = range_to_remove

        # A, B
        current_left, current_right = current_range = list.shift

        # A -> B
        # C -> D
        # Equal ranges
        return list if current_range == range_to_remove

        #         A -> B
        # C -> D
        return (stack + leftover_list) if current_left >= new_right

        # A -----------> B
        #     C --> D
        if (current_left < new_left) && (current_right > new_right)
          segment_one = [current_left, new_left]
          segment_two = [new_right, current_right]

          stack.push(segment_one, segment_two)
        end

        # A -> B
        #         C -> D
        if current_right <= new_left

          stack.push(current_range)

          return do_remove(stack, list, range_to_remove)
        end

        # A -------> B
        # C --> D
        if current_left == new_left
          new_range = [current_right, new_right].sort
          stack.push(new_range)

          return RangeList::Utils.maybe_append_list(stack, list)
        end

        # A -------> B
        #      C --> D
        if current_right == new_right
          new_range = [current_left, new_left].sort

          stack.push(new_range)
          return RangeList::Utils.maybe_append_list(stack, list)
        end

        # A -----> B     E -> F G ------> H
        #      C -----------------> D
        #
        # or
        #
        #    A -----> B     E -> F G ------> H
        # C -------------------------> D
        #
        if (current_right < new_right) && (current_right > new_left)
          if current_left < new_left
            segment_one = [current_left, new_left]
            stack.push(segment_one)
          end

          tmp_right = current_right
          until new_right < tmp_right
            # loop until we reach the range
            _tmp_left, tmp_right = list.shift
          end

          stack.push([new_right, tmp_right])

          return RangeList::Utils.maybe_append_list(stack, list)
        end

        # overlap
        #
        # Example
        #
        #    A ------> B
        # C ----> D
        #
        # or
        #
        # A ------> B
        #    C -------> D
        #
        # or
        #
        #    A ----> B
        # C -----------> D
        #
        if (current_left >= new_left) || (current_right <= new_right)
          segment_one = [current_left, new_left].sort
          segment_two = [current_right, new_right].sort

          puts segment_one
          puts segment_two

          stack.push(segment_one, segment_two)

          return RangeList::Utils.maybe_append_list(stack, list)
        end

        stack
      end
    end
  end
end
