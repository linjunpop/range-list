# frozen_string_literal: true

module RangeList
  # A module with some utiltiles methods
  module Utils
    class << self
      # append list to stack if the list is not empty
      def maybe_append_list(stack, list)
        if list.empty?
          stack
        else
          stack + list
        end
      end
    end
  end
end
