# frozen_string_literal: true

RSpec.describe RangeList::Adder do
  context "#add" do
    it "add a range to list" do
      expected = [[1, 5]]
      result = RangeList::Adder.add([], [1, 5])

      expect(result).to eq(expected)
    end

    it "add two ranges to list" do
      expected = [[1, 5], [10, 20]]
      result = RangeList::Adder.add([[1, 5]], [10, 20])

      expect(result).to eq(expected)
    end

    it "add a point to list" do
      expected = [[1, 5], [10, 20]]

      result = RangeList::Adder.add([[1, 5], [10, 20]], [20, 20])

      expect(result).to eq(expected)
    end

    it "add more" do
      expected = [[1, 5], [10, 25], [30, 50]]

      result = RangeList::Adder.add([[1, 5], [10, 20], [30, 50]], [11, 25])

      expect(result).to eq(expected)
    end

    it "add a invalid range to list" do
      expect do
        RangeList::Adder.add([], [5])
      end.to raise_error(RuntimeError, /invalid/)
    end
  end
end
