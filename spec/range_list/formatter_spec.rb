# frozen_string_literal: true

RSpec.describe RangeList::Formatter do
  context "#format_range" do
    it "format the range" do
      result = RangeList::Formatter.format_range([1, 2])
      expect(result).to eq([1, 2])
    end

    it "format the range" do
      result = RangeList::Formatter.format_range([2, 1])
      expect(result).to eq([1, 2])
    end
  end

  context "#validate_range" do
    it "allow valid input" do
      result = RangeList::Formatter.validate_range([1, 2])
      expect(result).to eq([1, 2])
    end

    it "raise if the range only has a single item" do
      expect do
        RangeList::Formatter.validate_range([5])
      end.to raise_error(RuntimeError, /invalid/)
    end

    it "raise if the range is empty" do
      expect do
        RangeList::Formatter.validate_range([])
      end.to raise_error(RuntimeError, /invalid/)
    end
  end
end
