# frozen_string_literal: true

RSpec.describe RangeList::List do
  before do
    @range_list = RangeList::List.new
  end

  context "#print" do
    it "print empty range list" do
      expected = ""
      result = @range_list.print

      expect(result).to eq(expected)
    end

    it "print the current range list" do
      range_list = RangeList::List.new(range: [1, 5])
      expected = "[1, 5)"
      result = range_list.print

      expect(result).to eq(expected)
    end
  end

  context "#add" do
    it "add a range to list" do
      expected = [[1, 5]]
      result = @range_list.add([1, 5])

      expect(result).to eq(expected)
    end

    it "add two ranges to list" do
      expected = [[1, 5], [10, 20]]
      @range_list.add([1, 5])
      result = @range_list.add([10, 20])

      expect(result).to eq(expected)
    end

    it "add a point to list" do
      expected = [[1, 5], [10, 20]]

      @range_list.add([1, 5])
      @range_list.add([10, 20])
      result = @range_list.add([20, 20])

      expect(result).to eq(expected)
    end

    it "add a invalid range to list" do
      expect do
        @range_list.add([5])
      end.to raise_error(RuntimeError, /invalid/)
    end
  end

  context "#remove" do
    it "remove range from list" do
      expected = []

      @range_list.add([1, 5])
      result = @range_list.remove([1, 5])

      expect(result).to eq(expected)
    end
  end

  context "suite" do
    it "run whole suite" do
      @range_list.add([1, 5])
      expect(@range_list.print).to eq("[1, 5)")

      @range_list.add([10, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")

      @range_list.add([20, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")

      @range_list.add([20, 21])
      expect(@range_list.print).to eq("[1, 5) [10, 21)")

      @range_list.add([2, 4])
      expect(@range_list.print).to eq("[1, 5) [10, 21)")

      @range_list.add([3, 8])
      expect(@range_list.print).to eq("[1, 8) [10, 21)")

      @range_list.remove([10, 10])
      expect(@range_list.print).to eq("[1, 8) [10, 21)")

      @range_list.remove([10, 11])
      expect(@range_list.print).to eq("[1, 8) [11, 21)")

      @range_list.remove([15, 17])
      expect(@range_list.print).to eq("[1, 8) [11, 15) [17, 21)")

      @range_list.remove([3, 19])
      expect(@range_list.print).to eq("[1, 3) [19, 21)")
    end
  end
end
