# frozen_string_literal: true

RSpec.describe RangeList::Remover do
  context "#remove" do
    it "remove the exact range from list" do
      expected = []
      result = RangeList::Remover.remove([[1, 5]], [1, 5])

      expect(result).to eq(expected)
    end

    it "remove an overlap range" do
      expected = [[1, 8], [11, 21]]
      result = RangeList::Remover.remove([[1, 8], [11, 21]], [10, 11])

      expect(result).to eq(expected)
    end

    it "remove another overlap range" do
      expected = [[1, 8], [11, 21]]
      result = RangeList::Remover.remove([[1, 8], [10, 21]], [10, 11])

      expect(result).to eq(expected)
    end

    it "remove another another overlap range" do
      expected = [[1, 8], [11, 15], [17, 21]]
      result = RangeList::Remover.remove([[1, 8], [11, 21]], [15, 17])

      expect(result).to eq(expected)
    end
  end
end
